#include <avr/io.h>
#include "system.h"


static void led_init (void)
{
    DDRC |= (1 << 2); // set the second bit of DDRC to output
}


static void led_on (void)
{
    PORTC |= (1 << 2); // turn on the light
}


static void led_off (void)
{
    PORTC &= (0 << 2); // turn off the light 
}



static void button_init (void)
{
    DDRD &= ~(1 << 7); // set the 7th bit of DDRD to input
}


static int button_pressed_p (void)
{
    return (PIND & (1 << 7)) != 0;
}


int main (void)
{
    system_init ();

    led_init ();
    button_init ();

    while (1)
    {
        if (button_pressed_p ())
        {
            led_on ();
        }
        else
        {
            led_off ();
        }
    }
}
