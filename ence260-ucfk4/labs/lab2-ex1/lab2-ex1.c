#include <avr/io.h>
#include "system.h"
#include "led.h"

#define HALF_A_SECOND 3906
#define A_SECOND 3906 * 2
#define SUPERFAST 8

int main (void)
{
    system_init ();
    led_init ();
    
    /* TODO: Initialise timer/counter1.  */
    TCCR1A = 0x00;
    TCCR1B = 0x05;
    // divide the main 8MHz clock frequency by 1024 for Timer/Counter1
    //
    // TCNT1 would increment every 1024 / 8*10^6 = 0.000128 seconds
    // This is the RESOLUTION of timer/counter
    //
    // We can get the value of TCNT1 by dividing the period by the resolution
    // 0.5 / 0.000128
    TCCR1C = 0x00;
    
    while (1)
    {
        
        /* Turn LED on.  */
        led_set (LED1, 1);
        TCNT1 = 0;
        while (TCNT1 < A_SECOND / 4) { // changed to superfast in 4.1
            // changed to A_SECOND in 4.2
            continue;
        }
        
        /* Turn LED off.  */
        led_set (LED1, 0);
        TCNT1 = 0;
        while (TCNT1 < A_SECOND * 3 / 4) { // changed to superfast in 4.1
            // changed to A_SECOND in 4.2
            continue;
        }
        
    }
    
}
