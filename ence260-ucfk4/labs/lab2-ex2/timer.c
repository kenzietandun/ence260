#include <avr/io.h>
#include "timer.h"

#define PRESCALER 1024.0
#define CLOCK_SPEED 8000000.0

/* Initialise timer.  */
void timer_init (void)
{
    TCCR1A = 0x00;
    TCCR1B = 0x05;
    TCCR1C = 0x00;
}


/* Wait for the specified length of time.  */
void timer_delay_ms (uint16_t milliseconds)
{
    /* TODO: Calculate the timer/counter value needed 
       for the given number of milliseconds. */
    uint16_t tcntNeeded = (milliseconds / 1000.0) / (PRESCALER / CLOCK_SPEED);

    /* TODO: Wait for the timer/couter to reach the 
       value calculated above.  */
    TCNT1 = 0;
    while (TCNT1 < tcntNeeded)
        continue;

}
