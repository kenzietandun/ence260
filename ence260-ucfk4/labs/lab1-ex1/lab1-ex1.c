#include <avr/io.h>
#include "system.h"

int main (void)
{
    system_init ();

    /* Initialise port to drive LED 1.  */
    DDRC |= (1 << 2); // set the second bit of DDRC to output
    // here we are setting the setting pin # 2 as output (high)
    
    DDRD &= ~(1 << 7); // set the 7th bit of DDRD to input
    
    while (1)
    {
        PORTC &= (0 << 2); // turn off the light 
        if ((PIND & (1 << 7)) != 0) { // check if the value of PIND is 1
            // PIND is 1 when the button is pressed
            PORTC |= (1 << 2); // turn on the light
        }
    }
}
