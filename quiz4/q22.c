#include <stdio.h>

char* mystrrchr(char *s, int c)
{
    char* found = NULL;
    // go to the end of string
    while (*s != '\0') {
        if (*s == c) {
            found = s;
        }
        s++;
    }
    return found;
}

int main(void)
{
    char* s = "TES123";
    char* foundAt = mystrrchr(s, 'S');
    if (foundAt == NULL) {
        puts("Not found");
    }
    else {
        printf("%zu\n", foundAt - s);
    }
}
