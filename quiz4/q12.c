#include <stdbool.h>
#include <stdio.h>

char data[100];
char thing[100];

bool isInData(char* p)
{
    return p >= &data[0] && p <= &data[99];
}

int main(void)
{
    printf("%d\n", isInData(&data[0]));

    printf("%d\n", isInData(&data[17]));

    printf("%d\n", isInData(&data[99]));

    printf("%d\n", isInData(&thing[99]));
}
