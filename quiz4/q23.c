#include <stdio.h>

int tokenCopy(char* dest, const char* src, int destSize)
{
    int i = 0;
    while (i < destSize - 1 && *(src + i) != '\0' && *(src + i) != ' ') {
        *(dest + i) = *(src + i);
        i++;
    }

    *(dest + i) = '\0';

    return i;
}

int main(void)
{
    char buff[5];
    int n = tokenCopy(buff, "Thi is a string", 5);
    printf("%d '%s'\n", n, buff);
}
