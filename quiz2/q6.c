#include <stdio.h>
#include <math.h>

#define DIFF b * b - 4 * a * c

int main(void)
{
    float a = 0;
    float b = 0;
    float c = 0;
    float x1 = 0;
    float x2 = 0;
    scanf("%f %f %f", &a, &b, &c);
    if (a == 0.0) {
        puts("Not a quadratic");
    } else if (DIFF < 0.0) {
        puts("Imaginary roots");
    } else {
        x2 = (-1 * b + sqrtf(DIFF)) / (2 * a);
        x1 = (-1 * b - sqrtf(DIFF)) / (2 * a);
        if (x1 < x2) {
            printf("Roots are %.4f and %.4f\n", x1, x2);
        } else {
            printf("Roots are %.4f and %.4f\n", x2, x1);
        }
    }
}
