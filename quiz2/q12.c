// Herbert, stop

#include <stdio.h>

int main(void)
{
    float screeHeight = 0.0;
    float rushHeight = 0.0;
    float slideBack = 0.0;
    scanf("%f %f %f", &screeHeight, &rushHeight, &slideBack);
    int num_rushes = 0;
    while (screeHeight > 0) {
        screeHeight -= rushHeight;
        num_rushes++;
        if (screeHeight > 0) {
            screeHeight += slideBack;
        }
    }

    printf("%d", num_rushes);
}
