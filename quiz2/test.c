#include <stdio.h>
#include <stdint.h>

int main(void)
{
    unsigned int n = 60000;
    printf("Big number = %u\n", n * 60000);

// find out how many bytes any variable occupies
// use sizeof operator

    printf("n occupies %zu bytes of memory\n", sizeof n);

    long long m = 0;
    printf("%zu bytes of memory\n", sizeof m);

    size_t o = 0;
    printf("size_t occupies %zu bytes of memory\n", sizeof o);
    printf("max range of size_t: %zu bits\n", sizeof o * 8);

    float k = 10.0;
    double l = 10.0;
    printf("%f\n", k);
    printf("%lf\n", l);
}
