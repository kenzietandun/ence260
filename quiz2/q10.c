#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int i = 0;
    bool done = false;
    for (i = 0; !done; i++) {
        if (i == 10) {
            done = true;
        }
    }

    printf("%d\n", i); // should print 11 
    // because the last update is still done after the condition check
}
