#include <stdio.h>

int main(void)
{
    int i = 1;
    int j = 10;
    int k = 100;

    // should print 1, 10, 100
    printf("Iniitially, i, j, k = %d, %d, %d\n", i, j, k);
    // j is now 1
    // i is now 2
    j = i++;
    printf("After j = i++, i, j, k = %d, %d, %d\n", i, j, k);
    // i is now 3
    // k is now 3
    k = ++i;
    // should print 3, 1, 3
    printf("After k = ++i, i, j, k = %d, %d, %d\n", i, j, k);
    // k is now 2
    // i is now 2
    i = --k;
    // should print 2, 1, 2
    printf("After i = --k, i, j, k = %d, %d, %d\n", i, j, k);
    // i is now 1
    // j is now 1
    j = --i;
    printf("After j = i--, i, j, k = %d, %d, %d\n", i, j, k);
    // j is now 20
    // k is now 30
    // i is now 50
    i = (j = 20) + (k = 30);
    // should print 50, 20, 30
    printf("i = (j = 20) + (k = 30), i, j, k = %d, %d, %d\n", i, j, k);
    return 0;
}
