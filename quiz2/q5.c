#include <stdio.h>

int main(void)
{
    int n = 0;
    scanf("%d", &n);
    if (n == 0) {
        puts("Zero");
    } else if (n % 2 == 0) {
        puts("Even");
    } else {
        puts("Odd");
    }
}
