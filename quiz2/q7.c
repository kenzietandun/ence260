#include <stdio.h>

int gringe(int boink, int flunk) {
    return boink == flunk ? 42 : flunk - 11;
}

int main(void)
{
    printf("%d\n", gringe(23, 23));
    printf("%d\n", gringe(23, 24));
}
