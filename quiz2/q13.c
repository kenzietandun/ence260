#include <stdio.h>
#include <math.h>

int main(void)
{
    int n1 = 0;
    int n2 = 0;

    scanf("%d %d", &n1, &n2);
    for (int i = n1; i <= n2; i++) {
        char is_prime = 1;
        for (int j = 2; j <= sqrt(i); j++) {
            if (i % j == 0) {
                is_prime = 0;
                break;
            }
        }

        if (is_prime) {
            printf("%d\n", i);
        }
    }
}
