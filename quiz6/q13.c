#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* skipping(const char* s)
{
    char* skipped = malloc(1);
    int i = 0;
    int j = 0;
    while (s[j] != '\0') {
        if (j % 2 == 0) {
            skipped = realloc(skipped, i + 2);
            skipped[i] = s[j];
            i++;
        }
        j++;
    }

    skipped = realloc(skipped, i + 1);
    skipped[i] = '\0';

    return skipped;
}

int main(void)
{
    char* s = skipping("nasi goreng");
    printf("%s\n", s);
    free(s);
}
