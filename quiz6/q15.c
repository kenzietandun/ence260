#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char* name;
    int age;
    double height;
} Person;

Person* newPerson(char* name, int age, double height)
{
    Person* pPerson = malloc(sizeof(Person));
    int nameLength = strlen(name);
    pPerson->name = malloc(nameLength + 1);
    strncpy(pPerson->name, name, nameLength);
    pPerson->name[nameLength] = '\0';
    pPerson->age = age;
    pPerson->height = height;

    return pPerson;
}

void freePerson(Person* person)
{
    free(person->name);
    free(person);
}

int main(void)
{
    Person* employee = newPerson("Billy", 30, 1.68);
    printf("%s is age %d and is %.2f m tall\n", employee->name, employee->age, employee->height);
    freePerson(employee);
}
