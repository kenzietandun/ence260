#include <stdio.h>
#include <stdlib.h>

int* ramp(int n)
{
    int* pNum = malloc(n * sizeof(int));
    for (int i = 0; i < n; i++) {
        *(pNum + i) = i + 1;
    }

    return pNum;
}

int main(void)
{
    int* data = ramp(5);
    for (int i = 0; i < 5; i++) {
        printf("%d ", data[i]);
    }
    free(data);
}
