#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

char* readLine(void)
{
    char* buff = NULL;
    int numBytes = 0;
    int c = 0;
    while ((c = getchar()) != EOF && c != '\n') {
        buff = realloc(buff, numBytes + 2);
        buff[numBytes++] = c;
    }

    if (buff != NULL) {
        buff[numBytes] = '\0';
    }

    return buff;
}

int main(void)
{
    char* line = NULL;
    bool done = false;
    while (!done) {
        puts("Enter a line of text");
        line = readLine();
        if (line != NULL) {
            printf("I got: %s\n", line);
            free(line);
        } else {
            done = true;
        }
    }
}
