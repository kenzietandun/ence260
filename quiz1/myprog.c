#include <stdio.h>

#define SCALE_FACTOR 1.609344

int main(void)
{
    float miles = 0.0;
    printf("How many miles? ");
    scanf("%f", &miles);
    printf("That's %.2f km.\n", SCALE_FACTOR * miles);
}
