/* prog.c
 * A variant of structexample3.c from lab 5 with the following changes:
 *
 * 1. The student struct now contains a firstname, a lastname and a student ID
 *    instead of a name and an age.
 * 2. The input data file is again a CSV file but the fields are firstname,
 *    lastname and student ID (an int).
 *
 * Richard Lobb, August 2019.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#define MAX_LINE_LENGTH 80      // The longest line this program will accept
#define MAX_NUM_STUDENTS 500    // The maximum number of students this program can handle
#define MAX_NAME_SIZE 50        // The maximum allowable name length

// The declaration of the student record (or struct). Note that
// the struct contains the first and last names as arrays of characters.

typedef struct student_s Student;

struct student_s {
    char firstname[MAX_NAME_SIZE];
    char lastname[MAX_NAME_SIZE];
    int studentId;
    Student* next;              // Pointer to next student in a list
};

// Create a pool of student records to be allocated on demand

Student studentPool[MAX_NUM_STUDENTS];  // The student pool
int firstFree = 0;

// returns true if student1 should "precede" student2 by name order
bool precedes(const Student* student1, const Student* student2)
{
    int fName_precede = strcmp(student1->firstname, student2->firstname);
    int lName_precede = strcmp(student1->lastname, student2->lastname);
    return fName_precede < 0 || (fName_precede == 0 && lName_precede < 0);
}

// inserts the new student in a sorted manner according to their name
Student* insert(Student* student, Student* list)
{
    Student* current = list; // pointer to current student, start from the head
    Student* prev = NULL; // pointer to prev student
    Student* head = list; // pointer to head of list
    // go to the location where the student should be placed
    while (current != NULL && precedes(current, student)) {
        prev = current;
        current = current->next;
    }

    if (current == NULL) { // at the end of the list
        prev->next = student;
    } else if (prev == NULL) { // head of the list
        student->next = current;
        head = student;
    }  else { // middle of the list
        prev->next = student;
        student->next = current;
    }
    return head;
}

// Return a pointer to a new student record from the pool, after
// filling in the provided first and last name and student ID fields.
// Returns NULL if the student pool is exhausted.
Student* newStudent(const char* firstname, const char* lastname, int studentId)
{
    Student* student = NULL;
    if (firstFree < MAX_NUM_STUDENTS) {
        student = &studentPool[firstFree];
        firstFree += 1;
        strncpy(student->firstname, firstname, MAX_NAME_SIZE);
        student->firstname[MAX_NAME_SIZE - 1] = '\0';  // Make sure it's terminated
        strncpy(student->lastname, lastname, MAX_NAME_SIZE);
        student->lastname[MAX_NAME_SIZE - 1] = '\0';  // Make sure it's terminated
        student->studentId = studentId;
        student->next = NULL;
    }
    return student;
}

// Read a single student from a csv input file with student first name in first column,
// second name in the second column and studentId in the last (third) column.
// Returns: A pointer to a Student record, or NULL if EOF occurs or if
// a line with fewer than 2 commas is read.
Student* readOneStudent(FILE* file)
{
    char buffer[MAX_LINE_LENGTH];
    Student* student = NULL;       // Pointer to a student record from the pool

    // read the current line to buffer
    char *line = fgets(buffer, MAX_LINE_LENGTH, file);
    if (line != NULL) {
        char* firstName = NULL;
        char* lastName = NULL;
        int age = 0;

        char* commaPos = strchr(line, ','); // find the first comma
        if (commaPos != NULL) {
            // set the first comma as terminator
            *commaPos = '\0'; // terminator for first name

            // set first name from line until the null char (first comma)
            firstName = line;
            commaPos++; // commaPos now points to start of last name
            lastName = commaPos;

            commaPos = strchr(commaPos, ',');
            if (commaPos != NULL) {
                *commaPos = '\0'; // terminator for last name

                commaPos++; // commaPos now points to start of age
                age = atoi(commaPos);
                student = newStudent(firstName, lastName, age);
            }
        }
    }

    return student;
}

// Reads a list of students from a given file. Input stops when
// a blank line is read, or an EOF occurs, or an illegal input
// line is encountered.
// Returns a pointer to the first student in the list or NULL if no
// valid student records could be read.
Student* readStudents(FILE *file)
{
    Student* first = NULL;     // Pointer to the first student in the list
    Student* student = readOneStudent(file);
    while (student != NULL) {
        if (first == NULL) {
            first = student;   // Empty list case
        } else {
            first = insert(student, first);
        }
        student = readOneStudent(file);
    }
    return first;
}

// printOneStudent: prints a single student, passed by value
void printOneStudent(Student student)
{
    printf("%s %s (%d)\n", student.firstname, student.lastname, student.studentId);
}


// printStudents: print all students in a list of students, passed
// by reference
void printStudents(const Student* student)
{
    while (student != NULL) {
        printOneStudent(*student);
        student = student->next;
    }
}

// Main program. Read a linked list of students from a csv file, then display
// the contents of that list.
int main(int argc, char* argv[])
{
    if (argc != 2) {
        fprintf(stderr, "Usage: prog filename\n");
    } else {
        char* filename = argv[1];
        FILE* inputFile = fopen(filename, "r");
        if (inputFile == NULL) {
            fprintf(stderr, "File '%s' not found\n", filename);
        } else {
            Student* studentList = readStudents(inputFile);
            printStudents(studentList);

            // The program could now do various things that make use of
            // the linked list, like deleting students and adding new ones,
            // but the program is already quite long enough!
        }
    }
}

