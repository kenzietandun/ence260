#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// Magic numbers
#define MAX_LEN_INPUT 500
#define MAX_LEN_STATION 30
#define MAX_LEN_DATE 9
#define MAX_NUM_RECORDINGS 366

// Define alias for recording struct
typedef struct recording_s Recording;

struct recording_s {
    int day;
    int month;
    int year;
    float temp;
    Recording* next;
};

Recording recordings[MAX_NUM_RECORDINGS]; // Array containing all the recordings
int firstFree = 0; // Counter to next available location in the array

char stationName[MAX_LEN_STATION];

/* Find the n-th occurence of character c in string s
 * Returns: pointer to the n-th occurence of c in s,
 *          NULL if not found*/
char* strchrn(char* s, int c, int n)
{
    char* chr = NULL;
    char* curr = s;
    int found = 0;
    while (*curr != '\0') {
        if (*curr == c) {
            found++;
        }

        if (found == n) {
            chr = curr;
            break;
        }
        curr++;
    }

    return chr;
}

/* Creates a new Recording,
 * Returns: NULL if there are no free slots in the array
 *          Pointer to new Recording otherwise*/
Recording* newRecording(char* date, float maxTemp)
{
    Recording* rec = NULL;
    if (firstFree < MAX_NUM_RECORDINGS) {
        rec = &recordings[firstFree++];

        char year[5] = {0};
        char month[3] = {0};
        char day[3] = {0};

        strncpy(year, date, 4);
        date += 4;
        strncpy(month, date, 2);
        date += 2;
        strncpy(day, date, 2);

        rec->year = atoi(year);
        rec->month = atoi(month);
        rec->day = atoi(day);

        rec->temp = maxTemp;
    }
    return rec;
}

/* Reads one Recording from the file,
 * Returns: NULL if the line is not a valid recording
 *          Pointer to Recording on the line */
Recording* readOneRecording(FILE* file)
{
    char buffer[MAX_LEN_INPUT];
    char* input = fgets(buffer, MAX_LEN_INPUT, file);
    char* date = NULL;
    float temp = 0;

    Recording* rec = NULL;
    if (input != NULL) {
        char* commaPos = strchrn(buffer, ',', 3);
        if (commaPos != NULL) {
            *commaPos = '\0';
            commaPos = strchrn(buffer, ',', 2);
            temp = atof(++commaPos);
            commaPos = strchrn(buffer, ':', 1);
            *commaPos = '\0';
            commaPos = strchrn(buffer, ',', 1);
            commaPos++;
            date = commaPos;
            rec = newRecording(date, temp);
        }
    }

    return rec;
}

/* Try to read Recording lines from the file, it will skip the first few */
/* lines until the word "Station" is found. */
/* Returns: pointer to the head of the Recording linked list */
Recording* readRecordings(FILE* file)
{
    Recording* head = NULL;
    Recording* tail = NULL;

    char buffer[MAX_LEN_INPUT] = {0};
    char* commaPos;
    while (strcmp("Station", buffer) != 0) {
        fgets(buffer, MAX_LEN_INPUT, file);
        commaPos = strchrn(buffer, ',', 1);
        if (commaPos != NULL) {
            *commaPos = '\0';
        }
    }

    Recording* rec = readOneRecording(file);
    while (rec != NULL) {
        // add it to the linked list
        if (head == NULL) {
            head = tail = rec;
        } else {
            tail->next = rec;
            tail = rec;
        }
        rec = readOneRecording(file);
    }

    return head;
}

/* Finds the station name from the file and saves it to
 * stationName varaible */
void getStationName(FILE* file)
{
    char buffer[MAX_LEN_INPUT];
    // ignore first 2 line and store 3rd line to buffer
    for (int i = 0; i < 3; i++) {
        fgets(buffer, MAX_LEN_INPUT, file);
    }
    char* commaPos = strchrn(buffer, ',', 1);
    *commaPos = '\0';
    strncpy(stationName, buffer, MAX_LEN_STATION);
}

/* Checks if the temperature is above the threshold
 * Returns: 1 if true, 0 otherwise */
int shouldPrint(float temp, float threshold)
{
    return temp > threshold;
}

/* Prints a single Recording */
void printSingleRecord(Recording* rec)
{
    printf("%02d/%02d/%d%9.1lf C\n",
           rec->day, rec->month, rec->year, rec->temp);
}

/* Prints the header */
void printHeader(float threshold)
{
    printf("Dates when %.1lf C was reached at %s\n", threshold, stationName);
    printf("\n");
    printf("   Date       MaxTemp\n");
}

/* Main function */
int main(int argc, char* argv[])
{
    if (argc != 3) {
        fprintf(stderr, "Usage: processtemps filename threshold\n");
    } else {
        char* filename = argv[1];
        float threshold = atof(argv[2]);
        FILE* file = fopen(filename, "r");
        if (file != NULL) {
            getStationName(file);
            printHeader(threshold);
            Recording* recs = readRecordings(file);
            Recording* head = recs;
            while (head != NULL) {
                if (shouldPrint(head->temp, threshold)) {
                    printSingleRecord(head);
                }
                head = head->next;
            }
        } else {
            fprintf(stderr, "File '%s' not found\n", filename);
        }
    }
}
