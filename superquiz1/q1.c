#include <stdio.h>

#define CRITICAL_HIGH 9.81
#define CRITICAL_LOW  5.0
#define MAX_SIZE      100000

int readDoubles(int n, double data[])
{
    int i = 0;
    double number = 0.0;
    while (i < n && scanf("%lf", &number) != EOF) {
        data[i++] = number;
    }

    return i;
}

void smoothData(int n, double data[])
{
    double prev1 = 0.0;
    for (int i = 0; i < n; i++) {
        if (i == 0) {
            prev1 = data[i];
            data[i] = (3 * data[i] + data[i+1]) / 4;
        } else if (i == (n - 1)) {
            data[i] = (3 * data[i] + prev1) / 4;
        } else {
            data[i] = (prev1 + 2 * data[i] + data[i+1]) / 4;
            prev1 = (4 * data[i] - prev1 - data[i+1]) / 2;
        }
    }
}

int main(void)
{
    double data[MAX_SIZE];
    int totalData = readDoubles(MAX_SIZE, data);
    smoothData(totalData, data);

    char isLow = 0;
    char isHigh = 0;
    double currTime = 0.0;
    double maxAcceleration = data[0];
    double maxAccelerationTime = 0.0;

    for (int j = 0; j < totalData; j++) {
        printf("%lf, ", data[j]);
    }
    for (int i = 0; i < totalData; i++) {
        if (data[i] > maxAcceleration) {
            maxAcceleration = data[i];
            maxAccelerationTime = currTime;
        }

        if (data[i] < CRITICAL_LOW) {
            isLow = 1;
            isHigh = 0;
        } else if (data[i] > CRITICAL_HIGH && isLow && !isHigh) {
            printf("Acceleration of %.2lf m/sec^2 exceeded at t = %.2lf secs.\n", CRITICAL_HIGH, currTime);
            isHigh = 1;
        }
        currTime += 0.01;

    }

    puts("");
    printf("Maximum acceleration: %.2lf m/sec^2 at t = %.2lf secs.", maxAcceleration, maxAccelerationTime);
}
