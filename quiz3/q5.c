#include <stdio.h>

int main(void)
{
    int c = 0;
    while ((c = getchar()) != EOF) {
        if (c == 10) { // newline
            printf("'\\n'");
        } else if (c >= 'a' && c <= 'z') {
            printf("'%c': Letter %d", c, c - 'a' + 1);
        } else if (c >= 'A' && c <= 'Z') {
            printf("'%c': Letter %d", c, c - 'A' + 1);
        } else if (c >= '0' && c <= '9') {
            printf("'%c': Digit %d", c, c - '1' + 1);
        } else {
            printf("'%c': Non-alphanumeric", c);
        }
        printf("\n");
    }
}
