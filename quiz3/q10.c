#include <stdio.h>

#define NUM_ALPHABET 26

int main(void)
{
    int c = 0;
    int letters[NUM_ALPHABET] = {0};
    while ((c = getchar()) != EOF) {
        if (c >= 'a' && c <= 'z') {
            letters[c - 'a']++;
        } else if (c >= 'A' && c <= 'Z') {
            letters[c - 'A']++;
        }
    }

    for (int i = 0; i < NUM_ALPHABET; i++) {
        printf("%c: %d\n", i + 'A', letters[i]);
    }
}
