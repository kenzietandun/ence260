#include <stdio.h>

int main(void)
{
    int c = 0;
    while ((c = getchar()) != EOF) {
        printf("%d\n", c);
    }
}
