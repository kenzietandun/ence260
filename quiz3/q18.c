#include <stdio.h>

int isWonRow(char player, char game[3][3], int rowNum)
{
    char won = 1;
    for (int j = 0; j < 3; j++) {
        if (game[rowNum][j] != player) {
            won = 0;
        }
    }
    return won;
}

int main(void)
{
    char game[3][3] = {{'X', 'O', ' '},{' ', ' ', ' '}, {'O', 'O', 'O'}};
    printf("%d\n", isWonRow('O', game, 0)); 
}
