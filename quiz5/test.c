#include <stdio.h>

int main(void) 
{
    char t[] = "test123";
    t[1] = '2';
    printf("%s", t); // this will print t2st123

    char* s = "test123";
    *++s = '2';
    printf("%s", s); // this will reach segfault

}
