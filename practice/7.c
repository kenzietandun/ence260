#include <stdio.h>

char* rotateLeft(char* s)
{
    char temp = *s;
    char* first = s;
    while (*s != '\0') {
        *s = *(s + 1);
        s++;
    }
    s--;
    *s = temp;
    return first;
}

int main(void)
{
    char s[] = "Hello World!";
    printf("%s\n", rotateLeft(s));
}
