#include <stdio.h>

int main(void)
{
    int sum = 0;
    int curr;
    int prev = 0;
    while (scanf("%d", &curr) != EOF && curr != 0) {
        if (curr == 13 || prev == 13) {
            prev = curr;
            continue;
        }

        sum += curr;
    }
    printf("%d", sum);
}
