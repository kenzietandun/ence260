#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char* description;
    float duration; // hours
    int priority;
} Task;

Task* newTask(char* description, float duration, int priority)
{
    Task* task = malloc(sizeof(Task));

    int descLen = strlen(description);
    task->description = malloc(descLen + 1);
    strncpy(task->description, description, descLen);
    task->description[descLen] = '\0';

    task->duration = duration;
    task->priority = priority;

    return task;
}

void freeTask(Task* task)
{
    free(task->description);
    free(task);
}

int main(void)
{
    Task* task = newTask("Studying for ENCE260", 2.5f, 1);
    printf("Task \'%s\' (priority %d) takes %.1f hours.\n", task->description, task->priority, task->duration);
    freeTask(task);
}


