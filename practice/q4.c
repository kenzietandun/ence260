#include <stdio.h>
#define MAX_LINE 100

int main(void)
{
    int num = 0;
    int sum = 0;
    int skip = 0;

    while(scanf("%d", &num) != EOF) {
        if (num == 0) {
            break;
        } else if (skip == 1) {
            skip = 0;
        } else if (num == 13) {
            skip = 1;
        } else {
            sum += num;
        }
    }

    printf("%d\n", sum);

}
