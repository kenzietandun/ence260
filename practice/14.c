#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char** split(const char* s)
{
    int i = 0;
    char** result = malloc(sizeof(char*));

    if (result != NULL) {
        while (*s != '\0') {
            while (*s == ' ') {
                s++;
            }
            char* spacePos = strchr(s, ' ');
            int wordLength = 0;
            if (spacePos != NULL) {
                wordLength = spacePos - s;
            } else { // we've reached the last word of the string
                wordLength = strlen(s);
            }
            result[i] = malloc(wordLength + 1);
            strncpy(result[i], s, wordLength);
            result[i][wordLength] = '\0';

            result = realloc(result, sizeof(char*) * (i+2));
            i++;

            s += wordLength;
        }
    }

    result[i] = NULL;
    return result;
}

int main(void)
{
    char** words = split("He said 'hello' to me!");
    int i = 0;
    while (words[i] != NULL) {
        puts(words[i]);
        free(words[i]);
        i += 1;
    }
    free(words);
}
