#include <stdio.h>

// ****************************
//
// Your answer is inserted here
//
// ****************************

typedef struct queue_s QueueElement;
struct queue_s {
    char* username;
    QueueElement* next;
};

QueueElement* appendToQueue(QueueElement* head, QueueElement* student)
{
    QueueElement* curr = head;

    if (curr == NULL) {
        return student;
    } else {
        while (curr->next != NULL) {
            curr = curr->next;
        }
        curr->next = student;
    }

    return head;
}

// Print the elements in the given queue of users preceded by the word
// 'Queue' and the supplied message.
void printQueue(QueueElement* queue, const char* message)
{
    QueueElement* current = queue;
    printf("Queue %s:", message);

    while (current != NULL) {
        printf(" %s", current->username);
        current = current->next;
    }
    printf("\n");
}


// Simple test of the student queue
int main(void)
{
    QueueElement* queue = NULL;
    QueueElement stud1 = {"abc24", NULL};
    QueueElement stud2 = {"pqr33", NULL};

    printQueue(queue, "at start");
    queue = appendToQueue(queue, &stud1);
    printQueue(queue, "after appending abc24");
    queue = appendToQueue(queue, &stud2);
    printQueue(queue, "after appending pqr33");
}

