#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct {
    char* description;
    float duration; // hours
    int priority;
} Task;

Task* newTask(char* description, float duration, int priority)
{
    Task* new = malloc(sizeof(Task));
    int descLength = strlen(description);
    new->description = malloc(descLength + 1);
    strncpy(new->description, description, descLength);
    new->description[descLength] = '\0';
    new->duration = duration;
    new->priority = priority;

    return new;
}

void freeTask(Task* task)
{
    free(task->description);
    free(task);
}

int main(void)
{
    Task* task = newTask("Studying for ENCE260", 2.5f, 1);
    printf("Task \'%s\' (priority %d) takes %.1f hours.\n", task->description, task->priority, task->duration);
    freeTask(task);
}
