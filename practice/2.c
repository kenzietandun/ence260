#include<stdio.h>

float ratio(int first, int second)
{
    return first * 1.0 / second;
}

int main(void)
{
    printf("%.3f\n", ratio(1, 2));
}
