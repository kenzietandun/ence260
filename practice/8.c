#include <stdio.h>
#include <string.h>

int countChars(const char* s, const char* charsOfInterest)
{
    int total = 0;
    char* charPos = NULL;
    while (*charsOfInterest != '\0') {
        const char* front = s;
        while ((charPos = strchr(front, *charsOfInterest)) != NULL) {
            charPos++;
            front = charPos;
            total++;
        }
        charsOfInterest++;
    }

    return total;
}

int main(void)
{
    printf("%d\n", countChars("ABBCCCDDD!", "BCD!5"));
}
