#include <stdio.h>
void swap2(int* x, int* y)
{
    if (*x > *y) {
        int temp = *x;
        *x = *y;
        *y = temp;
    }
}

void swap3(int* x, int* y, int* z)
{
    swap2(x, y);
    swap2(x, z);
    swap2(y, z);
}

int main(void)
{
    int a = 10;
    int b = 0;
    int c = 7;
    swap3(&a, &b, &c);
    printf("%d <= %d <= %d\n", a, b, c);
}
