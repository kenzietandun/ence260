#include <stdio.h>

int add_even_sub_odd(const int data[], int n)
{
    int sum = 0;
    for (int i = 0; i < n; i++) {
        if (data[i] % 2 == 0) {
            sum += data[i];
        } else {
            sum -= data[i];
        }
    }

    return sum;
}
