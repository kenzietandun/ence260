#include <stdio.h>

int mystrnlen(const char* s, int maxlen)
{
    int i = 0;
    while (s[i] != '\0') {
        if (i == maxlen) {
            return maxlen;
        }
        i++;
    }

    return i;
}

int main(void)
{
    printf("%d\n", mystrnlen("ENCE260", 8));
    printf("%d\n", mystrnlen("ENCE260", 6));
}
