#include <stdio.h>

int* findPair(int* data, int numEls, int first, int second)
{
    int* pos = NULL;
    for (int i = 0; i <= numEls - 2; i++) {
        if (data[i] == first && data[i+1] == second) {
            pos = data + i;
            break;
        }
    }

    return pos;
}

int main(void)
{
    int data[] = {1, 10, 3, 20, 1, 3, 7};
    int* p = findPair(data, 7, 1, 3);
    if (p != NULL) {
        printf("Found at position %zd\n", p - data);
    } else {
        puts("Not found");
    }
}
