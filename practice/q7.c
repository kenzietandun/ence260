#include <stdio.h>
#include <ctype.h>

char* alphaShiftRight(char* s)
{
    char* head = s;
    while (*s != '\0') {
        if (isalpha(*s)) {
            if (*s == 'z' || *s == 'Z') {
                *s = *s - 26;
            }
            *s = *s + 1;
        }
        s++;
    }
    return head;
}

int main(void)
{
    char s[] = "B hr mns rn azc zesdq zkk";
    printf("%s\n", alphaShiftRight(s));
}
