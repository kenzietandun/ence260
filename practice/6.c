#include <stdio.h>

void swap2(int* x, int* y)
{
    int temp;
    if (*x < *y) {
        temp = *x;
        *x = *y;
        *y = temp;
    }
}

void swap3(int* x, int* y, int* z)
{
    swap2(x, y);
    swap2(x, z);
    swap2(y, z);
}

int main(void)
{
    int a = 1;
    int b = 0;
    int c = 7;
    swap3(&a, &b, &c);
    printf("%d <= %d <= %d\n", c, b, a);
}
