#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char** split(const char* s)
{
    char** result = malloc(sizeof(char*));
    const char* curr = s;
    int i = 0;

    while (*curr != '\0') {
        while (*curr == ' ') {
            curr++;
        }

        char* nextSpace = strchr(curr, ' ');
        int length = 0;
        if (nextSpace != NULL) {
            length = nextSpace - curr;
        } else {
            length = strlen(curr);
        }

        result[i] = malloc(length + 1);
        strncpy(result[i], curr, length);
        result[i][length] = '\0';

        result = realloc(result, sizeof(char*) * (i + 2));
        i++;

        curr += length;
    }

    result[i] = NULL;
    return result;
}

int main(void)
{
    char** words = split("He said 'hello' to me!");
    int i = 0;
    while (words[i] != NULL) {
        puts(words[i]);
        free(words[i]);
        i += 1;
    }
    free(words);
}
