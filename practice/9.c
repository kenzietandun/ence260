#include <stdio.h>

int* findPair(int* data, int numEls, int first, int second)
{
    for (int i = 0; i <= numEls - 2; i++) {
        if (data[i] == first && data[i+1] == second) {
            return &data[i];
        }
    }

    return NULL;
}
